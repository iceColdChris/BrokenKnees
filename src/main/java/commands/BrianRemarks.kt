package commands

import com.github.kvnxiao.discord.meirei.annotations.Command
import com.github.kvnxiao.discord.meirei.annotations.CommandGroup
import com.github.kvnxiao.discord.meirei.command.CommandContext
import com.github.kvnxiao.discord.meirei.d4j.sendBuffered
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent

@CommandGroup("commands.brianRemarks")
class BrianRemarks {
    companion object {
        const val PREFIX = ""
    }


    @Command(
            id = "ok",
            aliases = ["ok", "Ok"],
            prefix = PREFIX
    )
    fun commandOk(context: CommandContext, event: MessageReceivedEvent) {
        event.channel.sendBuffered("Ok..")
    }
}