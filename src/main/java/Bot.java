import com.github.kvnxiao.discord.meirei.Meirei;
import com.github.kvnxiao.discord.meirei.d4j.MeireiD4J;
import commands.BrianRemarks;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;

public class Bot {

    public static Bot INSTANCE;
    public static IDiscordClient client;

    public static void main(String... args) {
        if(args.length < 1)
            throw new IllegalArgumentException("You must provide the bot token");

        INSTANCE = login(args[0]);

        Meirei meirei = new MeireiD4J(client);
        meirei.addAnnotatedCommands(new BrianRemarks());
    }

    public Bot(IDiscordClient client) {
        Bot.client = client;
    }

    private static Bot login(String token) {
        Bot b = null;

        ClientBuilder builder = new ClientBuilder();
        builder.withToken(token);

        try {
            IDiscordClient client = builder.login();
            b = new Bot(client);
        } catch (DiscordException ex) {
            System.err.println("Error occurred while logging in!");
            ex.printStackTrace();
        }

        return b;
    }
}
